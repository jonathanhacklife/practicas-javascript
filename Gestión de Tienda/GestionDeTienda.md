﻿# Gestión de tienda
Estás trabajando en un programa de gestión de tiendas, que almacena los precios en una matriz.
Necesitas añadir una funcionalidad para aumentar los precios en la cantidad dada.
La variable **increase** es tomada de la entrada del usuario. Es necesario aumentar todos los precios de la matriz dada en esa cantidad y entregar a la consola la matriz resultante.

**NOTA:**
Usa un bucle para iterar a través de la matriz y aumentar todos los elementos.

------------

## Código inicial:
```javascript
function aumentar(listaPrecios, aumento) {
}
```

## Tests:
- 9 => [107.99, 24.2, 29, 1035]
- 100 => [198.99, 115.2, 120, 1126]