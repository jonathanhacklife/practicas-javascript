﻿function main(profundidad) {
  // Contamos desde el paso 1 como referencia
  let pies = 1;

  // Por cada día que pase, mientras no haya sobrepasado la profundidad, seguirá contando
  for (let dias = 1; pies <= profundidad; dias++) {
    // El caracol sube 7 pies cada día y retrocede 2 pies cada noche
    pies += 7;
    if (pies > profundidad)
      return dias; // Termina la función y devuelve la cantidad de días que tomará salir del pozo
    else
      pies -= 2;
  }
}

// TESTs
console.log(main(31))
console.log(main(42))
console.log(main(128))