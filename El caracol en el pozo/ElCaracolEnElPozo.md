﻿# El caracol en el pozo
El caracol sube 7 pies cada día y retrocede 2 pies cada noche.

¿Cuántos días le tomará al caracol salir de un pozo con la profundidad dada?

**EJEMPLO DE ENTRADA:**
31

**EJEMPLO DE SALIDA:**
6


**EXPLICACIÓN:**
Desglosemos la distancia que el caracol cubre cada día:


Día 1: `7-2=5`
Día 2: `5+7-2=10`
Día 3: `10+7-2=15`
Día 4: `15+7-2=20`
Día 5: `20+7-2=25`
Día 6: `25+7=32`

Así que, en el día 6 el caracol alcanzará los 32 pies y saldrá del pozo durante el día, sin volver a resbalar esa noche.

------------

## Código inicial:
```javascript
function main(depth) {
  // Tu código va a aquí
}
```

## Tests:
- 31 => 6
- 42 => 8
- 128 => 26