// Definimos las variables en una sola línea
let notas = [];

class Nota {
  constructor(titulo, mensaje) {
    this.titulo = titulo;
    this.mensaje = mensaje;
  }
}

// Funcion para agregar al Notas
function crearNota() {
  let nuevaNota = new Nota(prompt("Ingrese el título de la nota"), prompt("Ingrese el mensaje de la nota"));

  if (nuevaNota.titulo.length > 0 && nuevaNota.mensaje.length > 0) {
    notas.push(nuevaNota);
    console.log(notas);
    document.getElementsByClassName("tituloNota")[0].innerHTML = "Título: " + notas[0].titulo;
    document.getElementById("cuerpoNota").innerHTML = "Mensaje: " + notas[0].mensaje;

    if (notas[notas.length - 1].titulo.length > 30) {
      document.getElementById("notas").innerHTML += `
    <a href="#" id="${[notas.length - 1]}" onclick="verNota(this.id)"><li>${notas[notas.length - 1].titulo.slice(0, 30)}...</li></a>
    `;
    } else {
      document.getElementById("notas").innerHTML += `
    <a href="#" id="${[notas.length - 1]}" onclick="verNota(this.id)"><li>${notas[notas.length - 1].titulo}</li></a>
      `;
    }
  }
}


function verNota(id) {
  document.getElementsByClassName("tituloNota")[0].innerHTML = "Título: " + notas[parseInt(id)].titulo;
  document.getElementById("cuerpoNota").innerHTML = "Mensaje: " + notas[parseInt(id)].mensaje;
}
