// Definimos las variables en una sola línea
let total = 0;
let precios = [];

// Funcion para agregar al carrito
function agregarAlCarrito() {
  var nuevoItem = parseInt(prompt("Ingrese el precio")); // Usamos parseInt para convertir la entrada de texto en un número
  if (!isNaN(nuevoItem)) { // Nos aseguramos de que el usuario ponga un número, sino, no hará nada.
    precios.push(nuevoItem); // Agregamos el nuevo ítem an la lista de precios
    console.log(precios);
    document.getElementById("carrito").innerHTML = "Sus productos: " + precios;
  }
}

// Funcion para ver el carrito
function verCarrito() {
  if (precios.length != 0) {
    // Obtenemos el ID "carrito" y reemplazamos su valor por el contenido de nuestra variable total.
    document.getElementById("carrito").innerHTML = "Total a pagar: $" + suma(precios);
    console.log("Carrito: " + precios);
  }
}

// Funcion para sumar precios
function suma(listaDePrecios) {
  total = 0; // Reseteamos el contador del total
  for (let i = 0; i < listaDePrecios.length; i++) { // Por cada ítem del 0 a la longitud de ítems...
    total += listaDePrecios[i]; // Agregamos el total + el nuevo precio
  };
  console.log("Total: " + total);
  return total;
}

// ----------------------

function crearElemento(texto) {
  // Crear nodo de tipo Element
  var parrafo = document.createElement("h1");

  // Crear nodo de tipo Text
  var contenido = document.createTextNode(texto);

  // Añadir el nodo Text como hijo del nodo tipo Element
  parrafo.appendChild(contenido);

  // Añadir el nodo Element como hijo de la pagina
  document.body.appendChild(parrafo);
}