﻿function convert(amount, rate) {
  return amount * rate
}

// TESTs
console.log(convert(100, 1.1))
console.log(convert(42, 0.72))
console.log(convert(1050, 9.4))