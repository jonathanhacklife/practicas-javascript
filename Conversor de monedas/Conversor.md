﻿# Conversor de monedas
Estás haciendo una app de conversión de monedas.
Crea una función llamada **convert**, que toma dos parámetros: la **cantidad a convertir**, y la **tasa**, y devuelve la cantidad resultante.
El código para tomar los parámetros como entrada y llamar a la función ya está presente más abajo.
Crea la función para hacer que el código funcione.

**EJEMPLO DE ENTRADA:**
100
1.1

**EJEMPLO DE SALIDA:**
110

**NOTA:**
La conversión de 100 a una tasa de 1.1 es igual a `100*1.1=110`

------------

## Código inicial:
```javascript
function convert(amount, rate) {
	// Tu código aquí
}
```

## Tests:
- 100 | 1.1 => 110
- 42 | 0.72 => 30.24
- 1050 | 9.4 => 9870