# Trabajo Integrador

En todas las consigna del curso se evaluará el trabajo asignando puntajes según: 

- Diseño 
- Implementacion de js
- Buenas prácticas


**Importante: En cada parte, sólo se podrá utilizar lo visto hasta el momento en el curso.**

## Primer Parte

**Dias: Jueves 11/03 y Martes 16/03**
Se requiere la implementación de los siguientes items en base a las tecnologías dictadas durante el curso:

- Landing page con colores de cys incorporado el menu navegable a calculadora cientifica y al conversor.
- Vista con la calculadora científica
- Vista con conversor de cripto divisas 
    - Btc a eth
    - Btc a usd
    - Btc a criptomoneda a elección
    - Btc a pesos arg

Se dispondrá 1 hora y 30 min para poder organizar y comenzar el desarrollo. En el caso de no terminarlo en ese tiempo, existe la posibilidad de poder continuarlo en las siguientes clases.

**Puntaje Total**

- Diseño 30 puntos
- Implementacion de js  40 puntos
- Buenas prácticas 30 puntos

## Segunda Parte 

**Dias: Jueves 18/03 y Martes 23/03**

- Calculadora: Idear eventos que disparen alguna visual al realizar una acción. Ejemplos: tocar 666 en la calculadora, realizar alguna operación que dé como resultado 666 + hacer aparecer imagen de chucky, cambiar de color todo el background a rojo, etc. A liberar la imaginación!

- Conversor:  

    - Realizar una vista donde se simule totalmente la compra y venta de divisas en el sistema.
    - Agregar dos divisas de propia autoría y los íconos de las ya existentes en el desarrollo.

**Puntaje Total**

- Diseño 20 puntos
- Implementacion de js  40 puntos
- Buenas prácticas 40 puntos