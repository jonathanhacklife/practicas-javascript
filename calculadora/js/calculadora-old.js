﻿// Seteamos el resultado en la pantalla
function setearResultado(valor) {
  document.getElementById("resultado").innerHTML = valor;
function agregarAlInput(valor) {
  document.getElementById("inputCalc").value = valor;
}

// Obtenemos el resultado del String
function obtenerResultado() {
  return (document.getElementById("inputCalc").value);
}

function agregar(boton) {
  var input = obtenerResultado();
  // Si el botón es un operador y el último caracter también, se reemplaza por el nuevo
  if (isNaN(boton) && isNaN(input.slice(-1))) agregarAlInput(input.slice(0, -1) + boton);
  else agregarAlInput(input + boton);
}

// Calculamos la operación del String
function calcular() {
  // Si el resultado es un número, calcula y lo muestra en pantalla
  if (!isNaN(obtenerResultado())) setearResultado(eval(obtenerResultado()));
  else setearResultado("Q no vs q es 1 calculadora invesil? Ponle numeritos...")
}

// Reseteamos el resultado para futuras operaciones
function resetear() {
  /*
  Para resetearlo le mandamos una cadena vacía
  al Input y a la etiqueta del resultado
  */
  agregarAlInput("");
  setearResultado("");
}