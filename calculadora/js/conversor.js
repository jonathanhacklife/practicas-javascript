// conversion ars -> usd
const ARS_USD = 1 / 140;      // 1 ars = 1/140 usd

// conversion btc -> usd
const BTC_USD = 56801.00;   // 1 btc = 56801.00 usd

// conversion eth -> usd
const ETH_USD = 1790.76;    // 1 eth = 1790.76 usd

// conversiones desde usd,ars,eth,btc a btc
const toBtc = {
    usd: 1 / BTC_USD,
    ars: ARS_USD / BTC_USD,
    eth: ETH_USD / BTC_USD,
    btc: 1
}

// conversiones desde usd,ars,eth,btc a eth
const toEth = {
    usd: 1 / ETH_USD,
    ars: ARS_USD / ETH_USD,
    eth: 1,
    btc: BTC_USD / ETH_USD
}

// conversiones desde usd,ars,eth,btc a ars
const toArs = {
    usd: 1 / ARS_USD,
    ars: 1,
    eth: ETH_USD / ARS_USD,
    btc: BTC_USD / ARS_USD
}

// conversiones desde usd,ars,eth,btc a usd
const toUsd = {
    usd: 1,
    ars: ARS_USD,
    eth: ETH_USD,
    btc: BTC_USD
}

// mapea las conversiones en general
const conversiones = {
    usd: toUsd,
    ars: toArs,
    btc: toBtc,
    eth: toEth
};

// se ejecuta al seleccionar una opcion nueva en el select (nueva moneda) 
// o al cambiar el input de texto 
function cambioFactor(e) {
    const monedaOrigen = document.getElementById("origen").value;
    const monedaDestino = document.getElementById("destino").value;
    const cantidad = parseInt(document.getElementById("cantidad").value);
    document.getElementById("resultados").innerHTML = conversiones[monedaDestino][monedaOrigen] * cantidad;
}

// toma las llaves presentes en el objeto de conversiones: usd,ars,btc,eth
// y crea opciones (en html) basandose en ellas
function crearMonedas() {
    const opciones = [];
    Object.keys(conversiones).forEach(
        function (currMoneda) {
            const opcion = document.createElement("option");
            const texto = document.createTextNode(currMoneda.toUpperCase());
            opcion.appendChild(texto);
            opcion.setAttribute("value", currMoneda);
            opciones.push(opcion);
        }
    )
    return opciones;
}

// toma las opciones creadas en crearMonedas() y las agrega a cada select
// clonandolas con cloneNode(), en el ultimo select toma las opciones reales 
//para eque no haga un clone de mas
function cargarDisponibles() {
    const select = document.getElementsByClassName("opciones-conv");
    const opciones = crearMonedas();
    [...select].map(function (currSelect, index) {
        const noEsFinal = index < select.length - 1;
        opciones.forEach(function (opc) {
            currSelect.appendChild(
                noEsFinal ? opc.cloneNode(true) : opc
            )
        })
    })
}

// ejecutar la carga de opciones una vez 
// que la ventana este cargada
window.onload = cargarDisponibles;