﻿let array = [
  {
    Estado: "Pendiente Firma Empleado",
    Documento: "Recibo de sueldo",
    Empleado: "Tomas",
    Sucursal: "GARIN",
    Gerencia: "Sistemas",
    Fecha: "01/02/2020",
  },
  {
    Estado: "Pendiente Firma Empleador",
    Documento: "Recibo de sueldo",
    Empleado: "Jonathan",
    Sucursal: "Floresta",
    Gerencia: "Sistemas",
    Fecha: "01/01/2020",
  },
  {
    Estado: "Notificación de vacaciones",
    Documento: "Notificacion",
    Empleado: "Gabriel",
    Sucursal: "GARIN",
    Gerencia: "Administración",
    Fecha: "02/02/2020",
  },
]


function filterTable(array, tipo, orden) {
  let lista = array.sort((a, b) => {
    if (a[tipo] > b[tipo]) {
      return 1;
    }
    if (a[tipo] < b[tipo]) {
      return -1;
    }
    // a must be equal to b
    return 0;
  })
  if (orden == "desc") {
    return lista.reverse();
  }
  else {
    return lista;
  }
}

console.log(filterTable(array, "Empleado", "asc"))
console.log(filterTable(array, "Fecha", "desc"))