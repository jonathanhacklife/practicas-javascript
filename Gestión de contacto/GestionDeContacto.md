﻿# Gestión de contacto
Estás trabajando en una app de gestión de contactos.
Has creado el objeto constructor contact, que tiene dos argumentos, **name** y **number**.
Necesitas añadir un método **print()** al objeto, que emitirá los datos de contacto a la consola en el siguiente formato: **name: number**.
El código dado declara dos objetos y llama a sus métodos print().
Completa el código definiendo el método print() para los objetos.

**NOTA:**
Observa el espacio después de los dos puntos al generar los datos de contacto.

------------

## Código inicial:
```javascript
function contact(name, number) {
	this.name = name;
	this.number = number;
}

var a = new contact("David", 12345);
var b = new contact("Amy", 987654321);
a.print();
b.print();
```

## Tests:
- David: 12345
- Amy: 987654321